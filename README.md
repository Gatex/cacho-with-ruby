##ABOUT 
  
***This is a simple implementation of one of the rules of a popular game in South America called "Cacho".***  
***It is written in Ruby for a purpose to learn fundamentals of Ruby programming language.***  
  
***This program contains such elements as:***  
	-Classes and objects  
	-Arrays  
	-'Rand' function for generating random integers used to immitate dice rolling  
	-File reading (each line in a file reading and splitting it to a 'words' array)  
	-User input (to choose either to immitate the game to find a sequence or to read sequences from array)  
	-'Sort' function for arrays sorting  
	-'While' and 'For' loops  
	-'If' conditional statements  
  
***This program is able to:***  
	-Immitate dice rolling and check if a sequence of 5 dices (or rolls) is an 'Escala' sequence.  
	-To read data file and check if sequences in it are 'Escala' sequences or not.  
  
***Idea for a program came from a problem solving task at:***  
	-https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=871&page=show_problem&problem=5041  
	  
##FUTURE PLANS  