#adding library for getting user's input
require 'io/console' 

puts "Welcome to Cacho!\n\n"
puts "Enter 1 to start searching for an 'escala' (rolling dices one by one)!\n\n"
puts "Enter 2 to start searching for an 'escala' (rolling all dices at the same time)!\n\n"
puts "Enter 3 to check for an 'escala' from file!\n\n"
#Waiting for user's input
method = gets.to_i

#Defining dice's value
class Dice	
	def roll
		@value = rand(1...7)
	end
	
	def get_value
		@value
	end
end

#Creating a dice object
dice = Dice.new
#Creating array for storing dice values
dice_values = Array.new(5)
#Creating a counter to count successfull rolls
rolling_success = false
#Creating a counter to count total games
total_games = 0

if method == 1 then
	#Loop for finding 'escala'
	while !rolling_success do
		#Single game's rolling loop
		puts "Rolling the dice"
		for i in 0..4
			#Rolling the dice
			dice.roll
			#Adding dice's value to array
			dice_values[i] = dice.get_value	
			#Checking if first dice roll was successfull
			if dice_values[0] > 2 then
				puts "Failed to find 'Escala' at roll: " + (i + 1).to_s
				puts dice_values
				puts "\n"
				dice_values.clear
				total_games += 1
				break
			end
			#Checking if the current dice's value is correct for 'Escala's' sequence 
			if i > 0 && dice_values[i] != (dice_values[i-1] + 1) then
				puts "Failed to find 'Escala' at roll: " + (i + 1).to_s
				puts dice_values
				puts "\n"
				dice_values.clear
				total_games += 1
				break
			end
			#Checking if the last dice's value of 'Escala's' sequence is correct
			if i==4 && dice_values[i] == (dice_values[i-1] + 1) then
				rolling_success = true
				puts "'Escala' found!"
				puts dice_values
				puts "\n"
				total_games += 1
				break
			end	
		end
	end
puts "Total games:" + total_games.to_s
end

if method == 2 then
	#Loop for finding 'escala'
	while !rolling_success do
		#Single game's rolling loop
		puts "Rolling the dice"
		for i in 0..4
			#Rolling the dice
			dice.roll
			#Adding dice's value to array
			dice_values[i] = dice.get_value	
		end
		#sorting dice values in ascending order
		dice_values = dice_values.sort
		#checking values from all dices
		for i in 0..4
			#Checking if there is a 1 or 2 in a sorted sequence
			if dice_values[0] > 2 then
				puts "Failed to find 'Escala' at dice no.: " + (i + 1).to_s
				puts dice_values
				puts "\n"
				dice_values.clear
				total_games += 1
				break
			#Checking if other dices values in a sequence are meeting the rule 
			elsif i > 0 && dice_values[i] != (dice_values[i-1].to_i + 1) then
				puts "Failed to find 'Escala' at dice no.: " + (i + 1).to_s
				puts dice_values
				puts "\n"
				dice_values.clear
				total_games += 1
				break
			end
			#Checking if the last dice's value in a sequence meets the rule
			if i==4 && dice_values[i] == (dice_values[i-1].to_i + 1) then
				rolling_success = true
				puts "'Escala' found!"
				puts dice_values
				puts "\n"
				total_games += 1
				break
			end	
		end
	end
puts "Total games:" + total_games.to_s
end

if method == 3 then
	words = Array.new(5)
	f = File.open("Data.txt", "r")
	f.each_line do |line|
		words = line.split
		if words.length == 1 then
			sequences = words[0]
		else
			for j in 0...words.length
				if j == 0 && words[j].to_i <= 2 then
					next
				elsif j == (words.length - 1) && words[j].to_i == (words[j-1].to_i + 1) then
					print words.to_s + " sequence is an 'Escala'!\n\n"
				elsif j > 0 && words[j].to_i == (words[j-1].to_i + 1) then
					next
				else
					print words.to_s + " sequence is not 'Escala'!\n\n"
					break
				end		
			end
		end
	end
	f.close
end